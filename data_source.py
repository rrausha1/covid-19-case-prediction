import pandas as pd
import global_var
import csv



def get_raw_data():

    df = pd.read_csv(global_var.url_confirmed)
    # reading the file and collecting data till 30th April
    country_wise_data = {}
    world_wide = []
    count_ite = 0
    count = 0
    pre = 0
    for i,j in df.iterrows():

        pre = 0
        if j[1] not in country_wise_data.keys():
            country_wise_data[j[1]] = []
            pre = 0
            for k in range(4,104):
                country_wise_data[j[1]].append(int(j[k])-pre)

                if count == 0:
                    world_wide.append(country_wise_data[j[1]][k-4])

                else:
                    world_wide[k-4] = world_wide[k-4] + int(j[k])-pre

                pre = int(j[k])
        else:
            pre = 0
            for k in range(4,104):
                country_wise_data[j[1]][k-4] = country_wise_data[j[1]][k-4]+int(j[k]) - pre

                if count == 0:
                    world_wide.append(country_wise_data[j[1]][k-4])

                else:
                    world_wide[k-4] = world_wide[k-4] + int(j[k]) - pre
                pre = int(j[k])


        count += 1
    # top counties list as per todatl number of cases
    top_country = [ 'US', 'Spain', 'Italy', 'Germany', 'France', 'China', 'Iran', 'United Kingdom',
                   'Turkey',
                   'Switzerland', 'Belgium', 'Netherlands', 'Canada', 'Austria', 'Portugal', 'Brazil',
                   'Israel', 'Sweden', 'Russia', 'Australia', 'Norway', 'Ireland', 'India', 'Chile', 'Denmark',
                   'Czechia',
                   'Poland', 'Japan', 'Romania', 'Pakistan', 'Malaysia', 'Philippines', 'Ecuador', 'Luxembourg',
                   'Saudi Arabia',
                   'Indonesia', 'Peru', 'Serbia', 'Mexico', 'Finland', 'Thailand', 'Panama', 'United Arab Emirates',
                   'Dominican Republic', 'Greece', 'Qatar', 'South Africa', 'Argentina', 'Iceland', 'Colombia']

     #filling the gap when nothing is reported between two  reported days
    for country in top_country:
        my_list = country_wise_data[country]
        for l in range(1,len(my_list)-2):
            if my_list[l] == 0 and my_list[l-1] !=0:
                if(my_list[l+1]%2==0):
                    my_list[l] = int(my_list[l+1]/2)
                else:
                    my_list[l] = int((my_list[l + 1] -1)/ 2)
                my_list[l+1] = int(my_list[l + 1] -my_list[l])


    # writing the data by creating the file country wise
    for row in sorted(country_wise_data.keys()):
        file_path = global_var.current_path+"/data/"+str(row)+".csv"
        with open(file_path, 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile)
            spamwriter.writerow(['counter','counter','data'])
            #spamwriter.writerow(country_wise_data[row])
            counter = 0
            for i in country_wise_data[row]:
                temp_string = [str(counter), str(counter), str(abs(i))]
                spamwriter.writerow(temp_string)
                counter = counter +1

    # writing the world wide data
    file_path = global_var.current_path + "/data/world_wide.csv"
    with open(file_path, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile)
        spamwriter.writerow(['counter','counter','data'])
        counter = 0
        for i in world_wide:
            temp_string = [str(counter),str(counter),str(i)]
            #temp_string = [str(counter),str(i)]
            spamwriter.writerow(temp_string)
            counter = counter+1


    #mainting the country list

    file_path = global_var.current_path + "/data/country_list.csv"
    with open(file_path, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile)
        spamwriter.writerow(['country_list'])
        spamwriter.writerow(['world_wide'])
        for i in country_wise_data.keys():
            spamwriter.writerow([i])

    print()

get_raw_data()

