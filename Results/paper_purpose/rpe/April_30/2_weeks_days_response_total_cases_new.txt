Country, total cases
US, 1069424.0
Spain, 213435.0
Italy, 205463.0
Germany, 163009.0
France, 167299.0
Iran, 94640.0
United Kingdom, 172481.0
Turkey, 120204.0
Switzerland, 29586.0
Belgium, 48519.0
Netherlands, 39512.0
Canada, 54457.0
Austria, 15452.0
Portugal, 25045.0
Brazil, 87187.0
Israel, 15946.0
Sweden, 21092.0
Russia, 106498.0
Australia, 6766.0
Norway, 7738.0
Ireland, 20612.0
Chile, 16023.0
Denmark, 9356.0
Czechia, 7682.0
Poland, 12877.0
Japan, 14088.0
Romania, 12240.0
Pakistan, 16817.0
Malaysia, 6002.0
Ecuador, 24934.0
Luxembourg, 3784.0
Saudi Arabia, 22753.0
Indonesia, 10118.0
Mexico, 19224.0
Finland, 4995.0
Thailand, 2954.0
Panama, 6532.0
United Arab Emirates, 12481.0
Singapore, 16169.0
Dominican Republic, 6972.0
Greece, 2591.0
South Africa, 5647.0
Argentina, 4428.0
Iceland, 1797.0
Colombia, 6507.0
India, 34863.0
Peru, 36976.0
Qatar, 13409.0
Philippines, 8488.0
Serbia, 9009.0
