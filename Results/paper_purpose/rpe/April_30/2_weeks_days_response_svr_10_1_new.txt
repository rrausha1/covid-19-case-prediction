Country, RPE_1, RPE_2, RPE_3, RPE_4, RPE_5
US, 12.74, 
Spain, 55.59, 
Italy, 15.25, 
Germany, 32.14, 
France, 4428.91, 
Iran, 10.68, 
United Kingdom, 13.39, 
Turkey, 17.04, 
Switzerland, 38.98, 
Belgium, 32.62, 
Netherlands, 28.2, 
Canada, 22.24, 
Austria, 43.75, 
Portugal, 54.6, 
Brazil, 25.12, 
Israel, 51.06, 
Sweden, 29.04, 
Russia, 5.0, 
Australia, 94.56, 
Norway, 60.71, 
Ireland, 47.08, 
Chile, 22.21, 
Denmark, 26.06, 
Czechia, 56.14, 
Poland, 26.07, 
Japan, 48.59, 
Romania, 30.0, 
Pakistan, 113.55, 
Malaysia, 45.19, 
Ecuador, 3568.17, 
Luxembourg, 94.9, 
Saudi Arabia, 12.41, 
Indonesia, 34.6, 
Mexico, 27.05, 
Finland, 58.22, 
Thailand, 38.19, 
Panama, 45.1, 
United Arab Emirates, 20.89, 
Singapore, 31.15, 
Dominican Republic, 56.12, 
Greece, 65.18, 
South Africa, 47.4, 
Argentina, 32.36, 
Iceland, inf, 
Colombia, 50.06, 
India, 24.14, 
Peru, 38.21, 
Qatar, 22.92, 
Philippines, 36.51, 
Serbia, 31.61, 
