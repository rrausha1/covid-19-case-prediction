import numpy as np
import sys
import global_var

sys.path.append(global_var.current_path+"/models")
import getSamples as gs
from sklearn import linear_model

def rpe(y_hat,y,yl):
	rpe_days = []
	rpe_days_result = []
	for i in range(len(y_hat)-2):
		rpe_days.append([])
		for j in range(len(y_hat[0])):
			if(y[i][j]!=0):
				rpe_days[i].append(abs(y_hat[i][j]-y[i][j])*100/y[i][j])
			else:
				rpe_days[i].append(abs(y_hat[i][j] - y[i][j]) * 100)
	if(yl == 3):
		rpe_days.append([])
		rpe_days[len(y_hat)-2].append(abs(y_hat[len(y_hat)-2][0]-y[len(y_hat)-2][0])*100/y[len(y_hat)-2][0])
		rpe_days[len(y_hat) - 2].append(
			abs(y_hat[len(y_hat) - 2][1] - y[len(y_hat) - 2][1]) * 100 / y[len(y_hat) - 2][1])
		rpe_days.append([])
		rpe_days[len(y_hat) - 1].append(
			abs(y_hat[len(y_hat) - 1][0] - y[len(y_hat) - 1][0]) * 100 / y[len(y_hat) - 1][0])
	else:
		rpe_days.append([])
		rpe_days[len(y_hat) - 2].append(
			abs(y_hat[len(y_hat) - 2][0] - y[len(y_hat) - 2][0]) * 100 / y[len(y_hat) - 2][0])


		rpe_days.append([])
		rpe_days[len(y_hat) - 1].append(
			abs(y_hat[len(y_hat) - 1][0] - y[len(y_hat) - 1][0]) * 100 / y[len(y_hat) - 1][0])

	if (y_length == 1):

		for j in range(len(y_hat[0])):
			sum_val = 0


			for i in range(len(y_hat)):
				sum_val+=rpe_days[i][j][0]

			rpe_days_result.append(round(sum_val/len(y_hat),2))
	else:
		sum_val = 0
		for i in range(len(y_hat)):
			sum_val += rpe_days[i][0][0]
		rpe_days_result.append(round(sum_val / len(y_hat), 2))

		sum_val = 0
		for i in range(len(y_hat)-1):
			sum_val += rpe_days[i][1][0]
		rpe_days_result.append(round(sum_val / (len(y_hat)-1), 2))

		sum_val = 0
		for i in range(len(y_hat) - 2):
			sum_val += rpe_days[i][2][0]
		if(len(y_hat)-2>0):
			rpe_days_result.append(round(sum_val / (len(y_hat) - 2), 2))
		else:
			rpe_days_result.append(1000)

	return rpe_days_result

historical_weeks = 2
predicted_weeks = 1

x_length = historical_weeks*7
y_length = predicted_weeks*3


data_path = global_var.current_path+'/../data/'

top_country = ['US','Spain','Italy','Germany','France','Iran', 'United Kingdom', 'Turkey',
                   'Switzerland','Belgium','Netherlands','Canada','Austria','Portugal','Brazil',
                   'Israel','Sweden','Russia','Australia','Norway','Ireland','Chile','Denmark','Czechia',
                   'Poland','Japan','Romania','Pakistan','Malaysia','Ecuador','Luxembourg','Saudi Arabia',
                   'Indonesia','Mexico','Finland','Thailand','Panama','United Arab Emirates','Singapore',
                   'Dominican Republic','Greece','South Africa','Argentina','Iceland','Colombia','India','Peru','Qatar','Philippines','Serbia']
fixed_per = [5,10,15,20]
num_pred = [1,3]
for per in fixed_per:
	for numpred in num_pred:

		print(len(top_country))
		rpe_day_path = global_var.current_path + "/../Results/paper_purpose/rpe/April_30/" + str(
			historical_weeks) + "_weeks_days_response_rd_"+str(per)+"_"+str(numpred)+"_new.txt"

		fh3 = open(rpe_day_path, "w")

		fh3.write("Country, RPE_1, RPE_2, RPE_3, RPE_4, RPE_5\n")

		for country in top_country:

			filename = data_path + country + '.csv'

			print("Running for:"+country+" , pred_length: "+str(numpred)+" , percentage:"+str(100-per))
			y_length = numpred
			_, __, ___, X_test, Y_test = gs.getSamples(filename, x_length, y_length)
			_, __, ___, X_test_i, Y_test_i = gs.getSamples(filename, x_length, 1)


			x = [i for i in range(0,x_length)]
			y = [i for i in range(x_length,x_length+y_length)]
			x_days = np.array(x)
			y_days = np.array(y)
			x_days = x_days.reshape((-1,1))
			y_days = y_days.reshape((-1,1))

			predictions = []
			test_temp1 =0
			test_temp2=[]
			max_diff = 0
			total_val = 0

			for i in range(len(X_test)-1):
				test_temp2.append(X_test[i, :].reshape((-1, 1))[0])
				total_val = total_val + test_temp2[i][0]
				if(i>0 and max_diff < abs(test_temp2[i-1]-test_temp2[i])):
					max_diff = abs(test_temp2[i-1]-test_temp2[i])
				if test_temp1 < max(X_test[i, :].reshape((-1, 1))):
					test_temp1 =max(X_test[i, :].reshape((-1, 1)))
			total_val = total_val + sum(Y_test[-1])+sum(X_test[-1])
			test_data = []
			test_data_y=[]
			m_value = total_val*per/100
			cont_val = 0
			sum1 = 0
			sum2 = 0
			sum3 = 0

			X_test_temp = []
			X_test_temp_i = []
			Y_test_temp = []
			Y_test_temp_i = []
			for i in range(len(X_test)):
				X_test_temp.append(X_test[i, :].reshape((-1, 1)))
				Y_test_temp.append(Y_test[i, :].reshape((-1, 1)))
			for i in range(len(X_test_i)):
				X_test_temp_i.append(X_test_i[i, :].reshape((-1, 1)))
				Y_test_temp_i.append(Y_test_i[i, :].reshape((-1, 1)))
			X_test = X_test_temp

			X_test_i = X_test_temp_i
			Y_test = Y_test_temp

			Y_test_i = Y_test_temp_i
			# Y_test_i[i] = Y_test_i[i, :].reshape((-1, 1))
			if y_length == 3:
				X_test.append(X_test_i[len(X_test_i) - 2])
				X_test.append(X_test_i[len(X_test_i) - 1])
				Y_test.append([Y_test_i[len(Y_test_i) - 2][0], Y_test_i[len(Y_test_i) - 1][0]])
				Y_test.append(Y_test_i[len(Y_test_i) - 1])

			for i in range(len(X_test)):
				test_temp = X_test[i]
				if (sum3+test_temp[0]>m_value):

					test_data.append(test_temp)
					test_data_y.append(Y_test[i])
					sum1 = sum1+test_temp[0]
					sum3 = sum3 + test_temp[0]

				else:
					sum2 = sum2+test_temp[0]
					cont_val = 1
					sum3 = sum3 + test_temp[0]
			model = linear_model.Ridge()
			for i in range(len(test_data)):
				model.fit(x_days,test_data[i])
				prediction = model.predict(y_days)
				predictions.append(prediction)


			predictions = np.array(predictions).reshape((-1,y_length))

			try:
				rpe_days = rpe(predictions,test_data_y,y_length)
			except:
				rpe_days = [1000,1000,1000]

			fh3.write(country + ', ')
			for r in range(len(rpe_days)):
				fh3.write(str(rpe_days[r]) + ', ')
			fh3.write('\n')

		fh3.close()
