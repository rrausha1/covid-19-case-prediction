import numpy as np
import sys
import global_var

sys.path.append(global_var.current_path + "/model")
import getSamples as gs
from pmdarima.arima import auto_arima
from sklearn import linear_model
from sklearn.svm import SVR


# ARIMA forcasting
def StartARIMAForecasting(Actual, number_of_prediction):
    model = auto_arima(Actual, trace=True, error_action='ignore', suppress_warnings=True)
    model.fit(Actual)
    forecast = model.predict(n_periods=number_of_prediction, alpha=0.001)
    forecast = [round(elem, 2) for elem in forecast]
    return forecast


# RPE calculation
def rpe(y_hat, y, yl):
    rpe_days = []
    rpe_days_result = []
    for i in range(len(y_hat) - 2):
        rpe_days.append([])
        for j in range(len(y_hat[0])):
            if (y[i][j] != 0):
                rpe_days[i].append(abs(y_hat[i][j] - y[i][j]) * 100 / y[i][j])
            else:
                rpe_days[i].append(abs(y_hat[i][j] - y[i][j]) * 100)
    # print(rpe_days[i][0])
    if (yl == 3):
        rpe_days.append([])
        rpe_days[len(y_hat) - 2].append(
            abs(y_hat[len(y_hat) - 2][0] - y[len(y_hat) - 2][0]) * 100 / y[len(y_hat) - 2][0])
        rpe_days[len(y_hat) - 2].append(
            abs(y_hat[len(y_hat) - 2][1] - y[len(y_hat) - 2][1]) * 100 / y[len(y_hat) - 2][1])
        rpe_days.append([])
        rpe_days[len(y_hat) - 1].append(
            abs(y_hat[len(y_hat) - 1][0] - y[len(y_hat) - 1][0]) * 100 / y[len(y_hat) - 1][0])
    else:
        rpe_days.append([])
        rpe_days[len(y_hat) - 2].append(
            abs(y_hat[len(y_hat) - 2][0] - y[len(y_hat) - 2][0]) * 100 / y[len(y_hat) - 2][0])

        rpe_days.append([])
        rpe_days[len(y_hat) - 1].append(
            abs(y_hat[len(y_hat) - 1][0] - y[len(y_hat) - 1][0]) * 100 / y[len(y_hat) - 1][0])

    if (y_length == 1):

        for j in range(len(y_hat[0])):
            sum_val = 0

            for i in range(len(y_hat)):
                sum_val += rpe_days[i][j][0]

            rpe_days_result.append(round(sum_val / len(y_hat), 2))
    else:
        sum_val = 0
        for i in range(len(y_hat)):
            sum_val += rpe_days[i][0][0]
        rpe_days_result.append(round(sum_val / len(y_hat), 2))

        sum_val = 0
        for i in range(len(y_hat) - 1):
            sum_val += rpe_days[i][1][0]
        rpe_days_result.append(round(sum_val / (len(y_hat) - 1), 2))

        sum_val = 0
        for i in range(len(y_hat) - 2):
            sum_val += rpe_days[i][2][0]
        if (len(y_hat) - 2 > 0):
            rpe_days_result.append(round(sum_val / (len(y_hat) - 2), 2))
        else:
            rpe_days_result.append(1000)

    return rpe_days_result


historical_weeks = 2
predicted_weeks = 1

x_length = historical_weeks * 7
y_length = predicted_weeks * 1
top_country = ['US', 'Spain', 'Italy', 'Germany', 'France', 'Iran', 'United Kingdom', 'Turkey',
               'Switzerland', 'Belgium', 'Netherlands', 'Canada', 'Austria', 'Portugal', 'Brazil',
               'Israel', 'Sweden', 'Russia', 'Australia', 'Norway', 'Ireland', 'Chile', 'Denmark', 'Czechia',
               'Poland', 'Japan', 'Romania', 'Pakistan', 'Malaysia', 'Ecuador', 'Luxembourg', 'Saudi Arabia',
               'Indonesia', 'Mexico', 'Finland', 'Thailand', 'Panama', 'United Arab Emirates', 'Singapore',
               'Dominican Republic', 'Greece', 'South Africa', 'Argentina', 'Iceland', 'Colombia', 'India', 'Peru',
               'Qatar', 'Philippines', 'Serbia']

data_path = global_var.current_path + '/../data/'

fixed_per = [5, 10, 15, 20]
num_pred = [1, 3]
for per in fixed_per:
    for numpred in num_pred:

        print(len(top_country))
        rpe_day_path = global_var.current_path + "/../Results/paper_purpose/rpe/April_30/" + str(
            historical_weeks) + "_weeks_days_response_ensemble_" + str(per) + "_" + str(numpred) + "_new.txt"
        fh3 = open(rpe_day_path, "w")

        fh3.write("Country, RPE_1, RPE_2, RPE_3, RPE_4, RPE_5\n")

        for country in top_country:

            filename = data_path + country + '.csv'

            print("Running for:" + country + " , pred_length: " + str(numpred) + " , percentage:" + str(100 - per))
            y_length = numpred

            response_times, X_train, Y_train, X_test, Y_test = gs.getSamples(filename, x_length, y_length)
            _, __, ___, X_test_i, Y_test_i = gs.getSamples(filename, x_length, 1)

            predictions = []
            predictions = []
            test_temp1 = 0
            test_temp2 = []
            max_diff = 0
            total_val = 0

            for i in range(len(X_test) - 1):
                test_temp2.append(X_test[i, :].reshape((-1, 1))[0])
                total_val = total_val + test_temp2[i][0]
                if (i > 0 and max_diff < abs(test_temp2[i - 1] - test_temp2[i])):
                    max_diff = abs(test_temp2[i - 1] - test_temp2[i])
                if test_temp1 < max(X_test[i, :].reshape((-1, 1))):
                    test_temp1 = max(X_test[i, :].reshape((-1, 1)))
            total_val = total_val + sum(Y_test[-1]) + sum(X_test[-1])

            test_data = []
            test_data_y = []
            m_value = total_val * per / 100

            cont_val = 0
            sum1 = 0
            sum2 = 0
            sum3 = 0
            X_test_temp = []
            X_test_temp_i = []
            Y_test_temp = []
            Y_test_temp_i = []
            for i in range(len(X_test)):
                X_test_temp.append(X_test[i, :].reshape((-1, 1)))
                Y_test_temp.append(Y_test[i, :].reshape((-1, 1)))
            for i in range(len(X_test_i)):
                X_test_temp_i.append(X_test_i[i, :].reshape((-1, 1)))
                Y_test_temp_i.append(Y_test_i[i, :].reshape((-1, 1)))
            X_test = X_test_temp

            X_test_i = X_test_temp_i
            Y_test = Y_test_temp

            Y_test_i = Y_test_temp_i

            if y_length == 3:
                X_test.append(X_test_i[len(X_test_i) - 2])
                X_test.append(X_test_i[len(X_test_i) - 1])
                Y_test.append([Y_test_i[len(Y_test_i) - 2][0], Y_test_i[len(Y_test_i) - 1][0]])
                Y_test.append(Y_test_i[len(Y_test_i) - 1])

            for i in range(len(X_test)):
                test_temp = X_test[i]

                if (sum3 + test_temp[0] > m_value):

                    test_data.append(test_temp)
                    test_data_y.append(Y_test[i])
                    sum1 = sum1 + test_temp[0]
                    sum3 = sum3 + test_temp[0]

                else:
                    sum2 = sum2 + test_temp[0]
                    cont_val = 1
                    sum3 = sum3 + test_temp[0]
            model1 = linear_model.Lasso()
            model2 = linear_model.Ridge()
            model3 = SVR(kernel='poly', C=100, gamma='auto', degree=2, epsilon=.1,
                         coef0=1)
            model4 = linear_model.LinearRegression()
            predictions_arima = []
            predictions_ridge = []
            predictions_lasso = []
            predictions_svr = []
            predictions_linear = []
            x1 = [i for i in range(0, x_length)]
            y1 = [i for i in range(x_length, x_length + y_length)]
            x_days1 = np.array(x1)
            y_days1 = np.array(y1)
            x_days1 = x_days1.reshape((-1, 1))
            y_days1 = y_days1.reshape((-1, 1))
            x2 = [i for i in range(0, x_length)]
            y2 = [i for i in range(x_length, x_length + y_length)]
            x_days2 = np.array(x2)
            y_days2 = np.array(y2)
            x_days2 = x_days2.reshape((-1, 1))
            y_days2 = y_days2.reshape((-1, 1))
            x3 = [i for i in range(0, x_length)]
            y3 = [i for i in range(x_length, x_length + y_length)]
            x_days3 = np.array(x3)
            y_days3 = np.array(y3)
            x_days3 = x_days3.reshape((-1, 1))
            y_days3 = y_days3.reshape((-1, 1))
            x4 = [i for i in range(0, x_length)]
            y4 = [i for i in range(x_length, x_length + y_length)]
            x_days4 = np.array(x4)
            y_days4 = np.array(y4)
            x_days4 = x_days4.reshape((-1, 1))
            y_days4 = y_days4.reshape((-1, 1))
            for i in range(0, len(test_data)):
                prediction_arima = StartARIMAForecasting(test_data[i], y_length)
                predictions_arima.append(prediction_arima)
                model2.fit(x_days2, test_data[i])
                prediction_ridge = model2.predict(y_days2)
                predictions_ridge.append(prediction_ridge)
                model1.fit(x_days1, test_data[i])
                prediction_lasso = model2.predict(y_days1)
                predictions_lasso.append(prediction_lasso)
                model3.fit(x_days3, test_data[i])
                prediction_svr = model3.predict(y_days3)
                predictions_svr.append(prediction_svr)
                model4.fit(x_days4, test_data[i])
                prediction_linear = model4.predict(y_days4)
                predictions_linear.append(prediction_linear)

            predictions_arima = np.array(predictions_arima).reshape((-1, y_length))
            predictions_ridge = np.array(predictions_ridge).reshape((-1, y_length))
            predictions_lasso = np.array(predictions_lasso).reshape((-1, y_length))
            predictions_svr = np.array(predictions_svr).reshape((-1, y_length))
            predictions_linear = np.array(predictions_linear).reshape((-1, y_length))

            try:
                rpe_days_arima = rpe(predictions_arima, test_data_y, y_length)
            except:
                rpe_days_arima = [1000, 1000, 1000]
            try:
                rpe_days_ridge = rpe(predictions_ridge, test_data_y, y_length)
            except:
                rpe_days_ridge = [1000, 1000, 1000]
            try:
                rpe_days_lasso = rpe(predictions_lasso, test_data_y, y_length)
            except:
                rpe_days_lasso = [1000, 1000, 1000]
            try:
                rpe_days_svr = rpe(predictions_svr, test_data_y, y_length)
            except:
                rpe_days_svr = [1000, 1000, 1000]
            try:
                rpe_days_linear = rpe(predictions_linear, test_data_y, y_length)
            except:
                rpe_days_linear = [1000, 1000, 1000]

            val_1 = []
            if (sum(rpe_days_arima) < sum(rpe_days_lasso) and sum(rpe_days_arima) < sum(rpe_days_ridge) and sum(
                    rpe_days_arima) < sum(rpe_days_svr) and sum(rpe_days_arima) < sum(rpe_days_linear)):
                val_1 = rpe_days_arima
            elif (sum(rpe_days_lasso) < sum(rpe_days_ridge) and sum(rpe_days_lasso) < sum(rpe_days_svr) and sum(
                    rpe_days_lasso) < sum(rpe_days_linear)):
                val_1 = rpe_days_lasso
            elif (sum(rpe_days_ridge) < sum(rpe_days_svr) and sum(rpe_days_ridge) < sum(rpe_days_linear)):
                val_1 = rpe_days_ridge
            elif (sum(rpe_days_svr) < sum(rpe_days_linear)):
                val_1 = rpe_days_svr
            else:
                val_1 = rpe_days_linear

            fh3.write(country)
            for r in range(len(val_1)):
                fh3.write(' & ' + str(val_1[r]))
            fh3.write('\n')

    fh3.close()
